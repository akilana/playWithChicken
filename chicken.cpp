#include "Chicken.h"

#include <iostream>

#define SEED_INIT_COUNT 5
#define EGG_COUNT 1

Chicken::Chicken()
{
	this->m_isAlive = true;
	this->m_isEgg = true;
	this->m_seedCount = SEED_INIT_COUNT;
}

void Chicken::printStatus()
{
	std::cout << "isAlive " << (this->m_isAlive ? "ALIVE" : "DEAD");
	std::cout << "; isEgg " << (this->m_isEgg ? "YES" : "NO");
	std::cout << "; seedCount " << this->m_seedCount << "\n \n";
}

void Chicken::killChicken()
{
	if (this->m_seedCount < 0)
	{
		this->m_isAlive = false;
	}
}

void Chicken::eatSeed()
{
	this->m_seedCount -= 1;
}

void Chicken::addSeed()
{
	this->m_seedCount += SEED_INIT_COUNT;
}

void Chicken::newEgg(int actionCount)
{
	if (actionCount%2 == 0 && !this->m_isEgg)
	{
		this->m_isEgg = true;
	}	
}

bool Chicken::getEgg()
{
	if (this->m_isEgg)
	{
		this->m_isEgg = false;
		return true;
	}
	else
	{
		return false;
	}
}