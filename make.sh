#!/bin/bash

INIT_FILE=("main" "Chicken" "function")

echo "------------------preproc and compile------------------"

for i in "${INIT_FILE[@]}"
do
	g++ -c $i.cpp -o $i.obj
	echo "g++ -c $i.cpp -o $i.obj"
done

echo "------------------linking------------------"

g++ `ls *.obj` -o game
echo "g++ `ls *.obj` -o game"

echo "echo ------------------done------------------"