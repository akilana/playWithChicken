#include <iostream>
#include "function.h"

void printUserRule()
{
	std::cout << "[" << ACTION_DO_NOTHING << "] Do nothing\n";
	std::cout << "[" << ACTION_GET_EGG << "] Get one egg\n";
	std::cout << "[" << ACTION_ADD_SEED << "] Add seed\n";
	std::cout << "[" << ACTION_EXIT << "] Exit\n";
}

int chickenId(int CHICKEN_COUNT)
{
	int chickenId = 0;
	int attempt = 1;
	while (attempt < 4)
	{
		std::cout << "Enter Chicken id\n";
		std::cin >> chickenId;
		
		if (chickenId > (CHICKEN_COUNT-1) || chickenId < 0)
		{
			std::cout << "Wrong chicken ID. Chicken Id must be from 0 to " << (CHICKEN_COUNT - 1) << "\n";
			std::cout << "You have 3 attempt. It is attempt #" << attempt << "\n";
			++attempt;
		}
		else
		{
			break;
		}
	}
	
	return chickenId;
}