struct Chicken
    {
        bool isAlive;
        bool isEgg;
        int seedCount;
    };


void getChickenInfo(int size, Chicken* chicken)
{
    for(int i = 0; i < size; i++)
    {
        std::cout << " Chicken number: " << i << " isAlive? " << chicken[i].isAlive << ", isEgg? " << chicken[i].isEgg << ", seedCount " << chicken[i].seedCount << "\n";  
    }
}
