#ifndef CHICKEN_H
#define CHICKEN_H

class Chicken
{
public:
	Chicken();
	void printStatus();
	bool getEgg();
	void addSeed();
	void eatSeed();
	void killChicken();
	void newEgg(int actionCount);
private:
	bool m_isAlive;
	bool m_isEgg;
	int m_seedCount;
	int* m_pointer;

};

#endif /* CHICKEN_H */