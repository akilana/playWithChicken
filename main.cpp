#include <iostream>
#include <string>
//#include "getGameRule.h"
//#include "getChickenInfo.h"
#include "Chicken.h"
#include "function.h"

int main(int argc, char** agrv)
{    		
	const int CHICKEN_COUNT = 3;

	Chicken chickenArray[CHICKEN_COUNT];
		
    int eggCount = 0;
    int actionCount = 0;
    
    int userAction = 0;

    while (userAction != ACTION_EXIT)
    {
    	printUserRule();
    	std::cout << "Please, Enter the action\n";
    	std::cin >> userAction;
    	++actionCount;
    	
    	/*each chicken eat 1 seed after every step and get new egg every positive step*/
    	for (int i = 0; i < CHICKEN_COUNT; ++i)
    	{
    		chickenArray[i].eatSeed();
    		chickenArray[i].newEgg(actionCount);
    	}
    	/* check chicken status: is Alive or not?*/
    	for (int i = 0; i < CHICKEN_COUNT; ++i)
    	{
    		chickenArray[i].killChicken();
    	}
    	    	
    	switch(userAction)
    	{
    		case ACTION_DO_NOTHING:
    		{
				break;
    		} 
    		case ACTION_ADD_SEED:
    		{
    			int chId = chickenId(CHICKEN_COUNT);
    			chickenArray[chId].addSeed();
    			break;
    		}  
    		case ACTION_GET_EGG:
    		{
    			int chId = chickenId(CHICKEN_COUNT);
    			if (chickenArray[chId].getEgg())
    			{
    				++eggCount;
    			}
    			break;
    		} 		    		
    	}
    	
    	for (int i = 0; i < CHICKEN_COUNT; ++i)
		{
			std::cout << "Chicken #" << i << "\n";
			chickenArray[i].printStatus();
		}
		std::cout << "----------------------------------------\n";
    }
}

